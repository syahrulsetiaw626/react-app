import './App.css';
import Router from './router';
import { Link } from 'react-router-dom';

function App() {
  return (
    
    <div className="">
      <div className="border-2 grid grid-cols-6 bg-slate-200 h-screen">
        <div className="p-8 bg-slate-50">
          <div className = "border-2 p-4 text-center" >
            <Link exact to={"/"} className="text-2xl font-bold">BALE WARGA 14</Link>
          </div>
          
          <div className="text-md text-slate-600 mt-12">Menu Utama</div>
          <Link exact to = {"/"} className = "w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Dashboard</Link>
          <Link to={"/about"} className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Data Aduan</Link>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Data Warga</a>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Cash Flow</a>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Informasi Umum</a>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Kelola Aset</a>
          <div className="text-md text-slate-600 mt-12">Menu Lainnya</div>
          <a href = "" className = "w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Settings</a>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Konfigurasi aplikasi</a>
          <a href="" className="w-full block text-xl p-2 px-4 hover:bg-red-200 rounded-lg font-bold text-left my-3">Log out</a>
        </div>
        <div className="col-span-5">
          <div className="p-4 bg-white border border-slate-600">
            <div className="flex justify-between items-center">
              <div className='w-4/12'>
                <input type="text" className="border p-2 px-4 w-full rounded-lg border-slate-400" placeholder='Cari Sesuatu?'/>
              </div>
              <div className="flex items-center">
                <div className="w-12 h-12 rounded border border-slate-600 mr-8"></div>
                <div className="w-10 h-10 bg-slate-600 rounded-full"></div>
                <div className="mx-2">Budi Sudarsono</div>
              </div>
            </div>
          </div>
          <main className='border -border-slate-800 p-4'>
            <div className="text-3xl font-bold">Reporting</div>
            <div className="text-small text-slate-600">Menampilkan Rekap Keuangan dan Aduan Masyarakat</div>
            {/* <Router></Router> */}
          </main>
        </div>
      </div>
    </div>
  );
}

export default App;
